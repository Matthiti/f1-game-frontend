import { createContext } from 'react';

const authContext = createContext({
  isLoggedIn: false,
  setIsLoggedIn: isLoggedIn => {}
});

export default authContext;