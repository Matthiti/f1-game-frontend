import { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function PlayerFormModal({ show, onClose, onSave, title, player = null }) {
  const [username, setUsername] = useState('');
  const [validated, setValidated] = useState(false);

  const beforeSave = e => {
    e.preventDefault();
    e.stopPropagation();

    const form = e.currentTarget;
    if (!form.checkValidity()) {
      setValidated(true);
      return;
    }

    setValidated(true);
    onSave({ username });
    resetForm();
  }

  const resetForm = () => {
    setUsername('');
    setValidated(false);
  }

  useEffect(() => {
    if (player) {
      setUsername(player.username);
    }
  }, [player])

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      
      <Modal.Body>
        <Form id="player-form" noValidate validated={validated} onSubmit={beforeSave}>
          <Form.Group controlId="username">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" value={username} required onChange={e => setUsername(e.target.value)}/>
            <Form.Control.Feedback type="invalid">
              Please enter a username.
            </Form.Control.Feedback>
          </Form.Group>
        </Form>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="outline-primary" onClick={onClose}>Close</Button>
        <Button variant="primary" form="player-form" type="submit">Save</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default PlayerFormModal;