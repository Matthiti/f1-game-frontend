FROM nginx:stable-alpine
RUN mkdir /app
COPY ./build /app
COPY nginx.conf /etc/nginx/nginx.conf

RUN touch /var/run/nginx.pid && chown -R nginx:nginx /var/run/nginx.pid /var/cache/nginx /etc/nginx/ /app

USER nginx

EXPOSE 8080
